# reduce(): reduce(fonksiyon, itarasyon yapılacak veri tipi(liste vb.))
from functools import reduce
sonuc = reduce(lambda x,y:x+y,[12,18,20,10])
print(sonuc)
# Sonuç 60. Önce 12 ile 18 toplandı. Sonra sırası ile sonuca 20 ve 10 eklenerek 60 sayısı elde edildi.
# sağlama "12 + 18 = 30", "30 + 20 =  50", "50 + 10 = 60"

# Maksimum bulma

def maksimum(x,y):
    if(x>y):
        return x
    else:
        return y

sonuc = reduce(maksimum,(-2,5,10,1,100))
print(sonuc)
#1. yol dongu ile
liste = ["Elma","Armut","Kiraz","Muz"]
sonuc = list()

i = 0

for a in liste:
    sonuc.append((i,a))
    i +=1
print(sonuc)
# çıktı : [(0, 'Elma'), (1, 'Armut'), (2, 'Kiraz'), (3, 'Muz')]

#2. yol enumerate ile
print(list(enumerate(liste)))
# çıktı : [(0, 'Elma'), (1, 'Armut'), (2, 'Kiraz'), (3, 'Muz')]

liste = ['a','b','c','d','e','f']

for index,eleman in enumerate(liste):
    if(index % 2 == 0):
        print('eleman: ',eleman)
sonuc = list(filter(lambda x : x % 2 == 0,[1,2,3,4,5,6,7,8,9,10]))
print(sonuc)
#sonuc [2, 4, 6, 8, 10]. listenin içerisinde 2 ye tam bölünenlerin (mod) listesini aldık


def asal_mi(x):
    i=2
    if(x == 1):
        return False
    elif(x == 2):
        return True
    else:
        while(i < x):
            if(x % i == 0):
                return False
            i+=1
        return True

print(asal_mi(1))
print(asal_mi(2))

sonuc = list(filter(asal_mi,(1,2,3,4,5,6,7,8,9,10,11)))
print(sonuc)
# sonuc [2, 3, 5, 7, 11]

sonuc = list(filter(asal_mi,range(1,100)))
print(sonuc)
# sonuc [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
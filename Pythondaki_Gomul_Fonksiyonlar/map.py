# map() : map(fonksiyon,itarasyon yapılacak veritipi (liste,demet,vb),...)
def double(x):
    return x * 2

liste = list(map(double,[1,2,3,4,5,6,7]))
print(liste)

# sonuç [2, 4, 6, 8, 10, 12, 14]

liste= list(map(lambda x : x ** 2,(1,2,3,4,5,6,7,8,9,10)))
print(liste)

# sonuç [1, 4, 9, 16, 25, 36, 49, 64, 81, 100] . lambda kullanarak her bir sayının karesi alınarak liste oluşturuldu
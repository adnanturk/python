print("""
*****************************
Faktöriyel bulma Programı

Çıkmak için q'ya basınız.
*****************************
""")

while True:
    sayi=input('Bir sayı giriniz:')
    if(sayi == 'q'):
        print('program sonlandırılıyor....')
        break
    else:
        sayi = int(sayi)

        faktoriyel = 1
        for i in range(2,sayi+1):
            faktoriyel = faktoriyel * i
        print('Faktöriyel:',faktoriyel)            


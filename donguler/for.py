# liste = [1,2,3,4,5,6,7,8,9,10]
# for eleman in liste:
#     if (eleman % 2 ==0):
#         print(eleman)

# jListe = [{1,2},{3,4},{5,6},{7,8}]        
# for eleman in jListe:
#     print(eleman)

# for (i,j) in jListe:
#     print('i:{}, j:{}'.format(i,j)) 


print("""
--Anahtarlar (keys)--
""")
sozluk = {'bir':1,'iki':2,'üç':3,'dört':4}
print(sozluk.keys())
for eleman in sozluk:
    print(eleman)

print("""
--Değerler (values)--
""")

for eleman in sozluk.values():
    print(eleman)    

print('-----------')
print(sozluk.items())


print("""
--Parçalar (items)--
""")

for (i,j) in sozluk.items():
    print('Anahtar:{}, Değer:{}'.format(i,j))

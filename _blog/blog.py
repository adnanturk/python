# Mustafa Murat Coşkun github https://bit.ly/2XkAFX8
# Message Flashing https://bit.ly/2IhiVUh
from flask import Flask, render_template, flash, redirect, url_for, session, request
from flask_mysqldb import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from functools import wraps  # Flask Decorator https://bit.ly/2WLzyM9


# Kullanıcı Giriş Decorator' ı
def login_required(f):  # f => decorator ün dahil edildiği fonksiyon (örn: dashboard)
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'logged_in' in session:
            # Eğer kullanıcı login ise f yani  örn: dashboard fonksiyonun tüm parametreler ile çalışması sağlanıyor
            return f(*args, **kwargs)
        else:
            flash('Bu sayfayı görüntülemek için lütfen giriş yapın!', 'danger')
            return redirect(url_for('login'))
    return decorated_function

# Kullanıcı Kayıt Formu


class RegisterForm(Form):
    name = StringField("İsim Soyisim", validators=[
                       validators.Length(min=4, max=25)])

    username = StringField("Kullanıcı Adı", validators=[
                           validators.Length(min=5, max=35)])

    email = StringField("Email Adresi", validators=[
                        validators.Email('Lütfen geçerli bir email giriniz!')])

    password = PasswordField("Parola", validators=[validators.DataRequired(
        message='Lütfen bir parola belirleyin'), validators.EqualTo(fieldname="confirm", message='Parolanız uyuşmuyor!')])

    confirm = PasswordField("Parola doğrula")

# Kullanıcı Giriş Formu


class LoginForm(Form):
    username = StringField("Kullanıcı Adı", validators=[
                           validators.DataRequired(message='Lütfen kullanıcı adı girin!')])

    password = PasswordField("Parola", validators=[
                             validators.DataRequired(message='Lütfen parola girin!')])


# Makale Kayıt Formu
class ArticleForm(Form):
    title = StringField("Makale Başlığı", validators=[
        validators.DataRequired(message='Lütfen makale başlığı girin!')])

    content = TextAreaField("İçerik", validators=[
        validators.Length(min=10)])


# http://flask.pocoo.org/
app = Flask(__name__)
# flash mesejların çalışması için uygulamaya secret_key ataması yapmamız gerekiyor. Bu key değeri kafadan atılabilir :)
app.secret_key = 'ybblog'
# mysql ayarları

app.config["MYSQL_HOST"] = "localhost"
app.config["MYSQL_USER"] = "root"
app.config["MYSQL_PASSWORD"] = ""
app.config["MYSQL_DB"] = "ybblog"
app.config["MYSQL_CURSORCLASS"] = "DictCursor"

mysql = MySQL(app)


@app.route("/")
def index():
    numbers = (1, 2, 3, 4, 5)

    names = [
        {"id": 1, "name": "Adnan TÜRK"},
        {"id": 1, "name": "Ezgi TÜRK"},
        {"id": 1, "name": "Devin Özgür TÜRK"},
    ]

    return render_template("index.html", answer="evet", numbers=numbers, names=names)


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/articles")
def articles():
    cursor = mysql.connection.cursor()
    query = 'Select * from articles'
    result = cursor.execute(query)
    if result > 0:
        articles = cursor.fetchall()
        return render_template("articles.html", articles=articles)
    else:
        flash('Henüz makale eklenmemiş!', 'info')
        return render_template("articles.html")


@app.route("/article/<string:id>")
def detail(id):
    cursor = mysql.connection.cursor()
    query = 'Select * from articles where id = %s'
    result = cursor.execute(query, (id,))

    if result > 0:
        article = cursor.fetchone()
        return render_template('detail.html', article=article)
    else:
        return render_template('detail.html')

# Kayıt İşlemi
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(form.password.data)

        cursor = mysql.connection.cursor()
        query = "INSERT INTO users(name,email,username,password) VALUES(%s,%s,%s,%s)"

        cursor.execute(query, (name, email, username, password))
        mysql.connection.commit()

        cursor.close()

        flash('Başarıyla kayıt oldunuz...', 'success')

        return redirect(url_for('login'))
    else:
        return render_template('register.html', form=form)
# WTForm template https://bit.ly/2Xgiq5f


# Giriş İşlemi
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)

    if request.method == 'POST' and form.validate():
        username = form.username.data
        password_entered = form.password.data

        cursor = mysql.connection.cursor()
        query = "SELECT * FROM users WHERE username = %s"  # AND password = %s"
        result = cursor.execute(query, (username,))

        if result > 0:
            data = cursor.fetchone()
            real_password = data["password"]
            if(sha256_crypt.verify(password_entered, real_password)):

                session["logged_in"] = True
                session["username"] = username

                flash('Giriş başarılı...', 'success')
                return redirect(url_for('index'))
            else:
                flash('Yanlış parola girdiniz...', 'danger')
                return redirect(url_for('login'))
        else:
            flash('Böyle bir kullanıcı yok...', 'danger')
            return redirect(url_for('login'))
    else:
        return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


@app.route('/dashboard')
@login_required
def dashboard():
    cursor = mysql.connection.cursor()
    query = 'Select * from articles where author = %s'
    result = cursor.execute(query, (session['username'],))

    if result > 0:
        articles = cursor.fetchall()
        return render_template('dashboard.html', articles=articles)
    else:
        return render_template('dashboard.html')


@app.route('/addarticle', methods=['GET', 'POST'])
@login_required
def addarticle():
    form = ArticleForm(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        content = form.content.data

        cursor = mysql.connection.cursor()
        query = 'Insert Into articles (title,author,content) values(%s,%s,%s)'

        cursor.execute(query, (title, session['username'], content))
        mysql.connection.commit()

        cursor.close()

        flash('Makale eklendi...', 'success')
        return redirect(url_for('dashboard'))

    else:
        return render_template('addarticle.html', form=form)


@app.route('/edit/<string:id>', methods=['GET', 'POST'])
@login_required
def edit(id):

    cursor = mysql.connection.cursor()
    query = 'Select * from articles where id = %s and author = %s'
    result = cursor.execute(query, (id, session['username']))

    if request.method == 'GET':
        if result > 0:
            article = cursor.fetchone()

            form = ArticleForm()
            form.title.data = article["title"]
            form.content.data = article["content"]

            return render_template('edit.html', form=form)
        else:
            flash('Makale bulunamadı', 'danger')
            return redirect(url_for('dashboard'))
    else: #Post Request
        form = ArticleForm(request.form)
        newTitle = form.title.data
        newContent = form.content.data

        query = 'Update articles SET title = %s, content = %s Where id = %s and author = %s'
        cursor.execute(query, (newTitle,newContent,id, session['username']))
        mysql.connection.commit()

        cursor.close()
        
        flash('Makale güncellendi', 'success')
        return redirect(url_for('dashboard'))

@app.route('/delete/<string:id>')
@login_required
def delete(id):
    cursor = mysql.connection.cursor()
    query = 'Delete from articles where id = %s and author = %s'
    result = cursor.execute(query, (id, session["username"]))

    if(result > 0):
        mysql.connection.commit()
        cursor.close()
        flash('Makale silindi...', 'success')
        return redirect(url_for('dashboard'))
    else:
        flash('Makale silinemedi', 'danger')
        return redirect(url_for('dashboard'))


if __name__ == "__main__":
    app.run(debug=True)

class Yazilimci():
    def __init__(self,isim,soyisim,numara,maas,diller):
        self.isim = isim
        self.soyisim = soyisim
        self.numara = numara,
        self.maas = maas
        self.diller = diller

    def bilgileriGoster(self):
      print("""
      Yazılımcı objesinin özellikleri

      İsim : {}

      Soyisim : {}

      Numara : {}

      Maaş : {}

      Diller : {}
      """.format(self.isim,self.soyisim,self.numara,self.maas,self.diller))
    
    def zamYap(self,zam_miktari):
        print('Zam yapılıyor....')
        self.maas +=zam_miktari
   
    def dilEkle(self,yeni_dil):
        print('Dil Ekleniyor....')
        self.diller.append(yeni_dil)
    
#yazilimci objesi
yazilimci1 = Yazilimci('Adnan','TÜRK','+905314956339',1000,['c#','python','javascript','jquery','react js'])

yazilimci1.bilgileriGoster()

yazilimci1.zamYap(1000)
yazilimci1.dilEkle('Cobol')

yazilimci1.bilgileriGoster()

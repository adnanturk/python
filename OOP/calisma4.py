#Interitance
class Calisan():
    def __init__(self,isim,maas,departman):
        self.isim = isim
        self.maas = maas
        self.departman = departman
    
    def bilgilerGosteriliyor(self):
        print('Çalışan sınıfının bilgileri......')
        print('İsim:{}\nMaaş:{}\nDepartman:{}'.format(self.isim,self.maas,self.departman))

    def departmanDegistir(self,yeni_departman):
        self.departman = yeni_departman

class Yonetici(Calisan):
    #class boş olduğu için hata vermemesi için pass komutunu kullanıyoruz. Class daha sonra geliştirilecek ise kullanılabilir.
    def zamYap(self,zam_miktari):
        self.maas +=zam_miktari

yonetici = Yonetici('Adnan TÜRK',3000,'Bilişim')


yonetici.bilgilerGosteriliyor()

yonetici.departmanDegistir('İnsan Kaynakları')
yonetici.zamYap(1500)

yonetici.bilgilerGosteriliyor()


        
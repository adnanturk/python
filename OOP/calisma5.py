#Interitance ve Override işlemi. (Super kelimesinin kullanımı)
# super anahtar kelimesi overrride edilen metodun da özelliklerini kullanmamızı sağlar!
class Calisan():
    def __init__(self,isim,maas,departman):
        print('Çalışan sınıfının init Fonksiyonu')
        self.isim = isim
        self.maas = maas
        self.departman = departman
    
    def bilgilerGosteriliyor(self):
      
        print('İsim:{}\nMaaş:{}\nDepartman:{}'.format(self.isim,self.maas,self.departman))

    def departmanDegistir(self,yeni_departman):
        self.departman = yeni_departman

class Yonetici(Calisan):
        #override
    def __init__(self,isim,maas,departman,kisi_sayisi):
        super().__init__(isim,maas,departman) 
        #calisan sınıfındaki init çağırılıyor ve değişkenleri kullanabilmek için parametreler göndreriliyor.
        print('Yönetici sınıfının init Fonksiyonu')
        
        # super anahtar kelimesi kullanmasaydık miraz alınan class ın property lerini kullanamicaktık. Ve aynılarını yazmak zorunda kalacaktık!
        # self.isim = isim
        # self.maas = maas
        # self.departman = departman
        self.kisi_sayisi = kisi_sayisi
    
    def bilgilerGosteriliyor(self):
      
        print('İsim:{}\nMaaş:{}\nDepartman:{}\nSorumlu Kişi Sayısı:{}'.format(self.isim,self.maas,self.departman,self.kisi_sayisi))

    def zamYap(self,zam_miktari):
        self.maas +=zam_miktari

yonetici = Yonetici('Adnan TÜRK',3000,'Bilişim',30)


yonetici.bilgilerGosteriliyor()

yonetici.departmanDegistir('İnsan Kaynakları')
yonetici.zamYap(1500)

yonetici.bilgilerGosteriliyor()


        
class  Animal():
    def __init__(self,name):
        self.name = name

    def saySomething(self):
        print("I am "+ self.name)

class Dog(Animal):
    def saySomething(self):
        print("I am "+ self.name\
            + ", and I can bark")

animal = Animal('Klosar')
animal.saySomething()

dog = Dog('Peti')
dog.saySomething()
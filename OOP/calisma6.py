# Özel metodlar: default olarak class oluşturulda python tarafından oluşturulan metodlar. örn dir(class_adi) yazarsak metodların listelerini görebiliriz.
# Bazı default tanımlanan özel metodlar:
# kitap = Kitap() __init__ 
# print(kitap) __str__
# len(kitap) __len__
# del kitap __del__
class Kitap():
    def __init__(self,isim,yazar,sayfa_sayisi,tur):
        self.isim = isim
        self.yazar=yazar
        self.sayfa_sayisi=sayfa_sayisi
        self.tur=tur

    def __str__(self):
        return 'İsim: {}\nYazar: {}\nSayfa Sayısı: {}\nTürü: {}'.format(self.isim,self.yazar,self.sayfa_sayisi,self.tur)

    def __len__(self):
        return self.sayfa_sayisi
    # del metodu farklı olarak override olmuyor sadece metoda ekstra özellik eklememize yarıyor. (del metodu biraz özel:) )
    def __del__(self):
        print('Kitap objesi siliniyor....')

kitap = Kitap('İstanbu Hatırası','Ahmet Ümit',561,'Polisiye')        
print(kitap)
print(len(kitap))
del kitap

print(kitap)

# Tüm özel metodlara ait liste http://getpython3.com/diveintopython3/special-method-names.html

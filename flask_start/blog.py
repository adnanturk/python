from flask import Flask, render_template

app = Flask(__name__)

# jinja template kullanılıyor
@app.route("/")
def index():

    # 1:
    # return 'Ana Sayfa'

    # 2:
    # sayi = 10
    # sayi2 = 15
    # return render_template(
    #     "index.html", number=sayi, number2=sayi2
    # )

    # 3:
    # article = dict()
    # article["title"] = "Article Title"
    # article["body"] = "Article Body"
    # article["author"] = "Article Author"
    # return render_template("index.html", article=article)

    return render_template("index.html")

    # render_template fonksiyonu templates klasöründeki html'leri bulup getiriyor.


@app.route("/about")
def about():
    return "Hakkımda"


@app.route("/about/adnan")
def adnan():
    return "Adnan TÜRK Hakkında"


# flask sayesinde sanal sunucu calıştırıyoruz
# aşağıdaki kod ile ilgili .py dosyasının bir terminal aracılığı ile mi çalıştırılıp çalıştırılmadığı kontrol ediliyor.
# Eğer terminalden çalıştırılmış ise __name__ değişkeni __main__ string ine eşit oluyor ve app.run fonksiyonu çalışıyor!
if __name__ == "__main__":
    app.run(debug=True)

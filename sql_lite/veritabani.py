import sqlite3

con = sqlite3.connect('kutuphane.db')
cursor = con.cursor()

def tablo_olustur():
    cursor.execute('CREATE TABLE IF NOT EXISTS kitaplik (isim TEXT, yazar TEXT, yayinevi TEXT, sayfa_sayisi INT)')
    con.commit()

def veri_ekle():
    cursor.execute('Insert Into kitaplik Values("İstanbul Hatırası","Ahmet Ümit","Everest","561")')
    con.commit()

def veri_ekle2(isim,yazar,yayin_evi,sayfa_sayisi=0):
    query='Insert Into kitaplik Values("{}","{}","{}",{})'.format(isim,yazar,yayin_evi,sayfa_sayisi)
    cursor.execute(query)
    #ya da
    #cursor.execute('Insert Into kitaplik Values(?,?,?,?)',(isim,yazar,yayin_evi,sayfa_sayisi))
    con.commit()
    print(isim)

def verileri_al():
    cursor.execute('Select * From kitaplik')
    data = cursor.fetchall()
    print('Kitaplık Tablosunun Bilgileri')
    for i in data:
        print(i)

def veri_getir(yayinEvi):
    cursor.execute('Select * From kitaplik where yayinevi = ?',(yayinEvi,))
    data = cursor.fetchall()
    for i in data:
        print(i)

#tablo_olustur()
#veri_ekle()

# isim = input('İsim:')
# yazar = input('Yazar:')
# yayin_evi = input('Yayın evi:')
# sayfa_sayisi = int(input('Sayfa sayisi:'))

# veri_ekle2(isim,yazar,yayin_evi,sayfa_sayisi)
# print('Kayit eklendi...')

# verileri_al()

veri_getir('Everest')
con.close()
import sqlite3
import time

class Kitap():
    def __init__(self,isim,yazar,yayinEvi,tur,baski):
            self.isim = isim
            self.yazar = yazar
            self.yayinEvi = yayinEvi
            self.tur = tur
            self.baski = baski
    def __str__(self):
        return 'İsim:{}\nYazar:{}\nYayın Evi:{}\nTür:{}\nBaskı:{}\n'.format(self.isim,self.yazar,self.yayinEvi,self.tur,self.baski)

class Kutuphane():
    def __init__(self):
        self.baglantiOlustur()
    
    def baglantiOlustur(self):
        self.baglanti = sqlite3.connect('kutuphane2.db')
        self.cursor = self.baglanti.cursor()
        query = 'Create Table If Not Exists kitaplar (isim TEXT, yazar TEXT, yayin_evi TEXT, tur TEXT, baski INT)'
        self.cursor.execute(query)
        self.baglanti.commit()
    
    def baglantiyiKes(self):
        self.baglanti.close()
    
    def kitaplariGetir(self):
        query='Select * From kitaplar'
        self.cursor.execute(query)
        kitaplar = self.cursor.fetchall()
        if (len(kitaplar) == 0):
            print('Kütüphanede kitap bulunmuyor')
        else:
            for item in kitaplar:
                kitap = Kitap(item[0],item[1],item[2],item[3],item[4])
                print(kitap)
    
    def kitapSorgula(self,isim):
        query = 'Select * from kitaplar Where isim = ?'
        self.cursor.execute(query,(isim,))
        kitaplar = self.cursor.fetchall()
        if(len(kitaplar)==0):
            print('Kitap bulunamadı')
        else:
            kitap = Kitap(kitaplar[0][0],kitaplar[0][1],kitaplar[0][2],kitaplar[0][3],kitaplar[0][4])
            print(kitap)

    def kitapEkle(self,kitap):
        query = 'Insert Into kitaplar values(?,?,?,?,?)'
        self.cursor.execute(query,(kitap.isim,kitap.yazar,kitap.yayinEvi,kitap.tur,kitap.baski))
        self.baglanti.commit()

    def kitapSil(self,isim):
        query = 'Delete from kitaplar Where isim = ?'
        self.cursor.execute(query,(isim,))
        self.baglanti.commit()

    def baskiYukselt(self,isim):
        query = 'Select * from kitaplar Where isim = ?'
        self.cursor.execute(query,(isim,))
        data = self.cursor.fetchall()
        if (len(data) == 0):
            print('Böyle bir kitap bulunmuyor.....')
        else:
            baski = data[0][4]
            baski +=1

            updateQuery = 'Update kitaplar Set baski = ? Where isim = ?'
            self.cursor.execute(updateQuery,(baski,isim))
            self.baglanti.commit()

                
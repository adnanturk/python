from kutuphane import *
print("""
Kütüphane Programına Hozgeldiniz.

İşlemler;

1. Kitapları Göster
2. Kitap Sorgulama
3. Kitap Ekle
4. Kitap Sil
5. Baskı Yükselt

Çıkmak için 'q' ya basın
***************************************************""")

kutuphane = Kutuphane()

while True:
    islem = input('Seçim yapın:')

    if (islem == 'q'):
        print('Program sonlandırılıyor.....')
        break
    elif (islem == '1'):
        kutuphane.kitaplariGetir()
        pass
    elif (islem == '2'):
        isim = input('Hangi kitabı istiyorsunuz? ')
        print('Kitap sorgulanıyor...')
        time.sleep(2)
        kutuphane.kitapSorgula(isim)
        pass
    elif (islem == '3'):        
        isim = input('Kitap adı:')
        yazar = input('Yazar:')
        yayinEvi = input('Yayın evi:')
        tur = input('Tür:')
        baski = int(input('Baskı:'))

        kitap = Kitap(isim,yazar,yayinEvi,tur,baski)

        print('Kitap ekleniyor....')
        time.sleep(2)
        kutuphane.kitapEkle(kitap)
        print('Kitap eklendi')        
    elif (islem == '4'):
        isim=input('Kitap ismi girin? ')
        print('Kitap siliniyor...')
        time.sleep(2)
        kutuphane.kitapSil(isim)
        print('Kitap silindi...')
    elif (islem == '5'):
        isim=input('Kitap ismi girin? ')
        print('Baskı yükseltiliyor')
        time.sleep(2)
        kutuphane.baskiYukselt(isim)
        print('Baskı yükseltildi....')
    else:
        print('Geçersiz işlem')
 # Hata fırlatmak: raise

def tersineCevir(s):
        if(type(s) !=str):
            raise ValueError('Lütfen doğru bir input girin')
        else:
            return s[::-1]
print(tersineCevir('Adnan'))


try:
    print(tersineCevir(1))
except:
    print('fonksiyon hata verdi...')

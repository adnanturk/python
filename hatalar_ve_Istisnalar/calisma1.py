#https://docs.python.org/3/tutorial/errors.html
try:
    print(a)    
except NameError as err:
    print('Hata',err)
    

try:
    a = int('sadsadasd')
except:
    print('Bir hata meydana geldi.')


try:
    a = int(input('sayı 1:'))
    b = int(input('sayı 2:'))
    print(a/b)
except ValueError:
    print('Lütfen girişleri kontrol edin')
except ZeroDivisionError:
    print("Bir sayı 0'a bölünemez")
# 2 except aynı anda da yazılabilir.
# except (ValueError,ZeroDivisionError):
#     print('ValueError veya ZeroDivisionError')


# ORM -> Flask-SQLAlchemy https://bit.ly/2x0xFQZ
from flask import Flask, render_template, url_for, redirect, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Git/calismalar/python/TodoApp/todo.db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Git/python/TodoApp/todo.db'

db = SQLAlchemy(app)


@app.route('/')
def index():
    todos = Todo.query.all()
    return render_template('index.html', todos=todos)

@app.route('/add', methods=['POST'])
def addTodo():
    title = request.form.get('title')
    newTodo = Todo(title=title, complate=False)
    db.session.add(newTodo)
    db.session.commit()
    return redirect(url_for('index'))

    # SQLITE tablo

@app.route('/update/<string:id>',methods = ['GET','POST'])
def update(id):
    todo = Todo.query.filter_by(id=id).first()
    # if todo.complate == True:
    #     todo.complate = False
    # else:
    #     todo.complate = True

    todo.complate = not todo.complate
    db.session.commit()

    return redirect(url_for('index'))

@app.route('/delete/<string:id>',methods = ['GET','POST'])
def delete(id):
    todo = Todo.query.filter_by(id=id).first()    
    db.session.delete(todo)
    db.session.commit()

    return redirect(url_for('index'))    

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))
    complate = db.Column(db.Boolean)
    #username = db.Column(db.String(80), unique=True, nullable=False)
    #email = db.Column(db.String(120), unique=True, nullable=False)


if __name__ == "__main__":
    # db.create_all() ile SQLITE database üzerine class ile tanımlanan tabloların oluşması sağlanıyor.
    # create_all komutu daha önce oluşturulmuş tablo var ise tabloların yeniden oluşturulmasına izin vermez.
    db.create_all()
    app.run(debug=True)

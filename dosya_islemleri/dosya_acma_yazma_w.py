# 'w' (write) ile dosya açılrsa ilgili dizinde dosyaolsun olması oluşturur.
# eğer daha önceden oluşturulmuş ise eskisi silinir yenisi oluşturulur.
file = open('/git/calismalar/python/dosya_islemleri/bilgiler.txt','w',encoding = 'utf-8')
file.write('Adnan TÜRK')
file.close()
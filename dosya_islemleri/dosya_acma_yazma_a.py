# 'a' (append) ile dosya açılrsa ilgili dizinde dosya yok ise yeni oluşturulur.
# eğer daha önceden oluşturulmuş ise dosya açılır ve en son satıra konumlanır.
file = open('/git/calismalar/python/dosya_islemleri/bilgiler.txt','a',encoding = 'utf-8')
file.write('Adnan TÜRK\n')
file.close()
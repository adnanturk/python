# *args ile bir demet (dizi) değer göndereceğimizi bildirebiliriz. Esnek sayıda parametre gönderebiliriz.
def fonksiyon(*args):
    for i in args:
        print(i)


fonksiyon(1,2,3)
fonksiyon(1,2,3,4,5,6)